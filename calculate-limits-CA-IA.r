# To run this script do:
# apt-get install r-base
# Rscript calculate-limits-CA-IA.r


# Define l, q and v
# Formulas and values are from https://tools.ietf.org/html/draft-irtf-cfrg-aead-limits-02 and
# https://datatracker.ietf.org/meeting/110/materials/slides-110-saag-analysis-of-usage-limits-of-aead-algorithms-00.pdf
l = 2^8
q = 2^20
v = 2^20

## AEAD_AES_128_GCM and AEAD_AES_256_GCM ##

# Calculate CA
# CA <= ((l + q)^2) / 2^129

ca = ((l + q)^2) / 2^129

print("AEAD_AES_128_GCM and AEAD_AES_256_GCM CA")
ca
print(paste0("2^", log2(ca)))

# Calculate IA
# IA <= v * l / 2^127

ia = v * l / 2^127

print("AEAD_AES_128_GCM and AEAD_AES_256_GCM IA")
ia
print(paste0("2^", log2(ia)))
print("---")
print("---")


## AEAD_CHACHA20_POLY1305 ##

# Calculate IA
# IA <= v * l / 2^103

ia = v * l / 2^103

print("AEAD_CHACHA20_POLY1305 IA")
ia
print(paste0("2^", log2(ia)))
print("---")
print("---")


## AEAD_AES_128_CCM ##

# Calculate CA
# CA <= l^2 * q^2 / 2^126

ca = l^2 * q^2 / 2^126

print("AEAD_AES_128_CCM CA")
ca
print(paste0("2^", log2(ca)))

# Calculate IA
# IA <= v / 2^128 + l^2(v + q)^2 / 2^126

ia = v / 2^128 + l^2 * (v + q)^2 / 2^126

print("AEAD_AES_128_CCM IA")
ia
print(paste0("2^", log2(ia)))
print("---")
print("---")


## AEAD_AES_128_CCM_8 ##

# Calculate CA
# CA <= l^2 * q^2 / 2^126

ca = l^2 * q^2 / 2^126

print("AEAD_AES_128_CCM_8 CA")
ca
print(paste0("2^", log2(ca)))

# Calculate IA
# IA <= v / 2^64 + l^2(v + q)^2 / 2^126

ia = v / 2^64 + l^2 * (v + q)^2 / 2^126

print("AEAD_AES_128_CCM_8 IA")
ia
print(paste0("2^", log2(ia)))
print("---")
print("---")

### End of initial section ###

## Calculations for AEAD_AES_128_CCM_8 with different q, v and l ##

func_aead_aes_128_ccm_8 <- function(q, v, l) {

  ## AEAD_AES_128_CCM_8 ##
  
  print(paste0("q: 2^", log2(q)))
  print(paste0("v: 2^", log2(v)))
  print(paste0("l: 2^", log2(l)))

  # Calculate CA
  # CA <= l^2 * q^2 / 2^126

  ca = l^2 * q^2 / 2^126

  print("AEAD_AES_128_CCM_8 CA")
  ca
  print(paste0("2^", log2(ca)))

  # Calculate IA
  # IA <= v / 2^64 + l^2(v + q)^2 / 2^126

  ia = v / 2^64 + l^2 * (v + q)^2 / 2^126

  print("AEAD_AES_128_CCM_8 IA")
  ia
  print(paste0("2^", log2(ia)))
  print("---")
  print("---")
}

func_aead_aes_128_ccm_8(2^20, 2^20, 2^8)
func_aead_aes_128_ccm_8(2^15, 2^20, 2^8)
func_aead_aes_128_ccm_8(2^10, 2^20, 2^8)
func_aead_aes_128_ccm_8(2^20, 2^15, 2^8)
func_aead_aes_128_ccm_8(2^15, 2^15, 2^8)
func_aead_aes_128_ccm_8(2^10, 2^15, 2^8)
func_aead_aes_128_ccm_8(2^20, 2^10, 2^8)
func_aead_aes_128_ccm_8(2^15, 2^10, 2^8)
func_aead_aes_128_ccm_8(2^10, 2^10, 2^8)

func_aead_aes_128_ccm_8(2^20, 2^20, 2^6)
func_aead_aes_128_ccm_8(2^15, 2^20, 2^6)
func_aead_aes_128_ccm_8(2^10, 2^20, 2^6)
func_aead_aes_128_ccm_8(2^20, 2^15, 2^6)
func_aead_aes_128_ccm_8(2^15, 2^15, 2^6)
func_aead_aes_128_ccm_8(2^10, 2^15, 2^6)
func_aead_aes_128_ccm_8(2^20, 2^10, 2^6)
func_aead_aes_128_ccm_8(2^15, 2^10, 2^6)
func_aead_aes_128_ccm_8(2^10, 2^10, 2^6)

