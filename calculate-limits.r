# To run this script do:
# apt-get install r-base
# Rscript calculate-limits.r


# Define l, p_q and p_v
# p_q and p_q are from https://tools.ietf.org/html/draft-ietf-tls-dtls13-41
# Formulas are from https://tools.ietf.org/html/draft-irtf-cfrg-aead-limits-02
l = 1024
p_q = 2^-60
p_v = 2^-57

## AEAD_AES_128_GCM and AEAD_AES_256_GCM ##

# q limit
# q <= (p^(1/2) * 2^(129/2) - 1) / (l + 1)

f <- function(q){
     (p_q^(1/2) * 2^(129/2) - 1) / (l + 1)  -  q
}
res = uniroot(f, interval = c(-1e+18, 1e+18))
q = res$root

print("AEAD_AES_128_GCM and AEAD_AES_256_GCM q limit")
q
print(paste0("2^", log2(q)))

# v limit
# v <= (p * 2^127) / (l + 1)

f <- function(v){
     (p_v * 2^127) / (l + 1)  -  v
}
res = uniroot(f, interval = c(-1e+28, 1e+28))
v = res$root

print("AEAD_AES_128_GCM and AEAD_AES_256_GCM v limit")
v
print(paste0("2^", log2(v)))
print("---")
print("---")

## AEAD_CHACHA20_POLY1305 ##

# v limit
# v <= (p * 2^103) / l

f <- function(v){
     (p_v * 2^103) / l  -  v
}
res = uniroot(f, interval = c(-1e+18, 1e+18))
v = res$root

print("AEAD_CHACHA20_POLY1305 v limit")
v
print(paste0("2^", log2(v)))
print("---")
print("---")


## AEAD_AES_128_CCM ##

# q limit
# q <= sqrt((p * 2^126) / l^2)

f <- function(q){
     sqrt((p_q * 2^126) / l^2)  -  q
}
res = uniroot(f, interval = c(-1e+18, 1e+18))
q = res$root

print("AEAD_AES_128_CCM q limit")
q
print(paste0("2^", log2(q)))

# v limit
# v + q <= p^(1/2) * 2^63 / l

f <- function(v){
     p_v^(1/2) * 2^63 / l  -  (v + q)
}
res = uniroot(f, interval = c(-1e+18, 1e+18))
v = res$root

print("AEAD_AES_128_CCM v limit")
v
print(paste0("2^", log2(v)))
print("---")
print("---")


## AEAD_AES_128_CCM_8 ##

# q limit (same as AEAD_AES_128_CCM)

print("AEAD_AES_128_CCM_8 q limit")
q

# v limit
# v * 2^64 + (2l * (v + q))^2 <= p * 2^128
# (TODO: Find way to solve here)

v = 58720256/(262145 + 2 * sqrt(17180000258))

print("AEAD_AES_128_CCM_8 v limit")
v
print(paste0("2^", log2(v)))
