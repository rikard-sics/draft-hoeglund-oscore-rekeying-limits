# Key Update for OSCORE

This is the working area for the internet-draft, "Key Update for OSCORE".

* [Editor's Copy](https://rikard-sics.gitlab.io/draft-hoeglund-oscore-rekeying-limits/)
* [Individual Draft](https://tools.ietf.org/html/draft-hoeglund-core-oscore-key-limits)
* [Compare Editor's Copy to Individual Draft](https://https:rikard-sics.gitlab.io/gitlab.com/#go.draft-hoeglund-oscore-rekeying-limits.diff) 


## Building the Draft

Formatted text and HTML versions of the draft can be built using `make`.

```sh
$ make
```

This requires that you have the necessary software installed.  See
[the instructions](https://github.com/martinthomson/i-d-template/blob/master/doc/SETUP.md).


## Contributing

See the
[guidelines for contributions](https://github.com/https:/gitlab.com/blob/master/CONTRIBUTING.md).

